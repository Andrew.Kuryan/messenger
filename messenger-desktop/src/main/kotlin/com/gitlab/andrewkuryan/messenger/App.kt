package com.gitlab.andrewkuryan.messenger

import javafx.geometry.Pos
import tornadofx.*

class MessengerApp: App(MainView::class, MainStyles::class)

class MainView: View() {

    private val lib = JVMLibrary()

    override val root = flowpane {
        addClass(MainStyles.mainRoot)
        vbox {
            addClass(MainStyles.infoBox)
            label(lib.rootAppName) {
                textFill = c("#5F9EA0")
            }
            label(lib.appName) {
                textFill = c("#FF3211")
            }
            label("Messenger Desktop") {
                textFill = c("#486FA0")
            }
        }
    }
}

class MainStyles: Stylesheet() {
    companion object {
        val mainRoot by cssclass()
        val infoBox by cssclass()
    }

    init {
        mainRoot {
            prefWidth = 500.px
            prefHeight = 600.px
            alignment = Pos.CENTER
            infoBox {
                alignment = Pos.TOP_CENTER
                spacing = 10.px
                fontSize = 20.px
            }
        }
    }
}
