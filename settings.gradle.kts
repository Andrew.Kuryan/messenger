rootProject.name = "messenger"

pluginManagement {

    repositories {
        jcenter()
        mavenCentral()
        gradlePluginPortal()
        google()
    }

    resolutionStrategy {
        eachPlugin {
            if (requested.id.id == "com.android.application") {
                useModule("com.android.tools.build:gradle:4.0.0-alpha08")
            }
        }
    }
}

include("messenger-common")
include("messenger-jvm")
include("messenger-js")
include("messenger-desktop")
include("messenger-server")
include("messenger-android")
