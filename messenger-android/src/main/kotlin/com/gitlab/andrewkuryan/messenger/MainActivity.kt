package com.gitlab.andrewkuryan.messenger

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.ui.core.*
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.material.MaterialTheme
import androidx.ui.text.TextStyle
import androidx.ui.tooling.preview.Preview

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            mainView(lib = JVMLibrary())
        }
    }
}

@Preview
@Composable
fun mainPreview() = mainView(lib = JVMLibrary())

@Composable
fun mainView(lib: JVMLibrary) {
    MaterialTheme() {
        Center {
            Column(crossAxisAlignment = CrossAxisAlignment.Center) {
                infoText(text = lib.rootAppName, color = 0xFF5F9EA0)
                HeightSpacer(height = 10.dp)
                infoText(text = lib.appName, color = 0xFFFF3211)
                HeightSpacer(height = 10.dp)
                infoText(text = "Messenger Android", color = 0xFF00574B)
            }
        }
    }
}

@Composable
fun infoText(text: String, color: Long) {
    Text(text, style = TextStyle(
            fontSize = 20.sp,
            color = Color(color)
    ))
}