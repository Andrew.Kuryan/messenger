import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.61"
    application
}

repositories {
    jcenter()
    maven(url = "http://dl.bintray.com/snimavat/maven")
}

val compileKotlin: KotlinCompile by tasks
val compileTestKotlin: KotlinCompile by tasks

compileKotlin.kotlinOptions.jvmTarget = "1.8"
compileKotlin.kotlinOptions.freeCompilerArgs = listOf("-Xallow-result-return-type")
compileTestKotlin.kotlinOptions.jvmTarget = "1.8"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation(project(":messenger-common"))
    implementation(project(":messenger-jvm"))

    implementation("com.soywiz.korlibs.klock:klock:1.8.4")
    implementation("org.jetbrains.exposed:exposed:0.17.7")
    implementation("me.nimavat:shortid:1.0.1.RC1")
    implementation("org.postgresql:postgresql:42.2.9")

    implementation("io.ktor:ktor-server-core:1.2.4")
    implementation("io.ktor:ktor-server-netty:1.2.4")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.5.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.2")
    testRuntimeOnly("org.junit.platform:junit-platform-console:1.2.0")

    testImplementation("io.kotlintest:kotlintest-core:3.3.2")
    testImplementation("io.kotlintest:kotlintest-assertions:3.3.2")
    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.3.2")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

application {
    mainClassName = "com.gitlab.andrewkuryan.messenger.AppKt"
}
