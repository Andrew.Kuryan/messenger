package com.gitlab.andrewkuryan.messenger.db

import com.gitlab.andrewkuryan.messenger.entity.UserData
import com.gitlab.andrewkuryan.messenger.entity.UserFilter
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.fail
import kotlin.test.assertEquals

class UsersRepositoryTest {

    private val repository = ExposedUsersRepository(
            Database.connect(
                    url = "jdbc:postgresql:messenger",
                    driver = "org.postgresql.Driver",
                    user = "postgres",
                    password = "admin"
            )
    )

    @Test
    fun testCreateUser() {
        val user = assertDoesNotThrow {
            runBlocking {
                repository.createUserAsync(UserData("Test", "Test"))
                        .await()
            }
        }
        runBlocking {
            repository.removeUserAsync(user.id).await()
        }
    }

    @Test
    fun testGetUserById() = runBlocking {
        val user = repository.createUserAsync(UserData("Test", "Test"))
                .await()
        try {
            val selectedUser = repository.getByIdAsync(user.id).await()
            assertEquals(user.id, selectedUser.id)
        } catch (exc: Exception) {
            repository.removeUserAsync(user.id).await()
            fail(exc)
        }
        repository.removeUserAsync(user.id).await()
        Unit
    }

    @Test
    fun testUpdateUser() = runBlocking {
        val oldUser = repository.createUserAsync(UserData("Test", "Test"))
                .await()
        try {
            repository.updateUserAsync(oldUser.id, UserData("New Test", "Test"))
                    .await()
            val newUser = repository.getByIdAsync(oldUser.id).await()
            assertEquals("New Test", newUser.userData.firstName)
        } catch (exc: Exception) {
            repository.removeUserAsync(oldUser.id).await()
            fail(exc)
        }
        repository.removeUserAsync(oldUser.id).await()
        Unit
    }

    @Test
    fun testSearchUsers() = runBlocking {
        val user1 = repository.createUserAsync(UserData("Test1", "Test1"))
                .await()
        val user2 = repository.createUserAsync(UserData("Test2", "Test2"))
                .await()
        val user3 = repository.createUserAsync(UserData("User", "User"))
                .await()

        try {
            val users = repository.searchUsersAsync(
                    UserFilter(namePattern = Regex("Test\\d"))
            ).await()
            assertEquals(2, users.size)
        } catch (exc: Exception) {
            repository.removeUserAsync(user1.id).await()
            repository.removeUserAsync(user2.id).await()
            repository.removeUserAsync(user3.id).await()
            fail(exc)
        }

        repository.removeUserAsync(user1.id).await()
        repository.removeUserAsync(user2.id).await()
        repository.removeUserAsync(user3.id).await()
        Unit
    }
}

//16
