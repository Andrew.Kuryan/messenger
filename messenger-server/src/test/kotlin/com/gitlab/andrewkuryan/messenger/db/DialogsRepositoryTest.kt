package com.gitlab.andrewkuryan.messenger.db

import com.gitlab.andrewkuryan.messenger.entity.DialogData
import com.gitlab.andrewkuryan.messenger.entity.UserData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import kotlin.test.assertEquals

class DialogsRepositoryTest {

    private val db = Database.connect(
            url = "jdbc:postgresql:messenger",
            driver = "org.postgresql.Driver",
            user = "postgres",
            password = "admin"
    )

    private val dialogsRepository = ExposedDialogsRepository(db)
    private val usersRepository = ExposedUsersRepository(db)

    @Test
    fun testCreateDialog() = runBlocking {
        val user1 = usersRepository.createUserAsync(UserData("First Test", "Test")).await()
        val user2 = usersRepository.createUserAsync(UserData("Second Test", "Test")).await()
        fun onEndAsync() = CoroutineScope(Dispatchers.IO).async {
            usersRepository.removeUserAsync(user1.id).await()
            usersRepository.removeUserAsync(user2.id).await()
        }
        try {
            val dialog = dialogsRepository.createDialogAsync(DialogData(user1, user2)).await()
            dialogsRepository.removeDialogAsync(dialog.id).await()
        } catch (exc: Exception) {
            onEndAsync().await()
            fail(exc)
        }
        onEndAsync().await()
        Unit
    }

    @Test
    fun testGetDialogById() = runBlocking {
        val user1 = usersRepository.createUserAsync(UserData("First Test", "Test")).await()
        val user2 = usersRepository.createUserAsync(UserData("Second Test", "Test")).await()
        val dialog = dialogsRepository.createDialogAsync(DialogData(user1, user2)).await()
        fun onEndAsync() = CoroutineScope(Dispatchers.IO).async {
            dialogsRepository.removeDialogAsync(dialog.id).await()
            usersRepository.removeUserAsync(user1.id).await()
            usersRepository.removeUserAsync(user2.id).await()
        }
        try {
            val result = dialogsRepository.getByIdAsync(dialog.id).await()
            with(result.dialogData.firstUser) {
                assertEquals("First Test", userData.firstName)
                assertEquals("Test", userData.lastName)
            }
            with(result.dialogData.secondUser) {
                assertEquals("Second Test", userData.firstName)
                assertEquals("Test", userData.lastName)
            }
        } catch (exc: Exception) {
            onEndAsync().await()
            fail(exc)
        }
        onEndAsync().await()
        Unit
    }

    @Test
    fun testGetUserDialogs() = runBlocking {
        val user1 = usersRepository.createUserAsync(UserData("First Test", "Test")).await()
        val user2 = usersRepository.createUserAsync(UserData("Second Test", "Test")).await()
        val user3 = usersRepository.createUserAsync(UserData("Third Test", "Test")).await()
        val dialog1 = dialogsRepository.createDialogAsync(DialogData(user1, user2)).await()
        val dialog2 = dialogsRepository.createDialogAsync(DialogData(user1, user3)).await()
        fun onEndAsync() = CoroutineScope(Dispatchers.IO).async {
            dialogsRepository.removeDialogAsync(dialog1.id).await()
            dialogsRepository.removeDialogAsync(dialog2.id).await()
            usersRepository.removeUserAsync(user1.id).await()
            usersRepository.removeUserAsync(user2.id).await()
            usersRepository.removeUserAsync(user3.id).await()
        }
        try {
            val dialogs = dialogsRepository.getUserDialogsAsync(user1.id).await()
            assertEquals(2, dialogs.size)
        } catch (exc: Exception) {
            onEndAsync().await()
        }
        onEndAsync().await()
        Unit
    }
}
