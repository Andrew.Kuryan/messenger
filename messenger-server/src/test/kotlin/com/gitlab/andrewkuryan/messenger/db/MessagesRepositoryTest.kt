package com.gitlab.andrewkuryan.messenger.db

import com.gitlab.andrewkuryan.messenger.entity.DialogData
import com.gitlab.andrewkuryan.messenger.entity.MessageData
import com.gitlab.andrewkuryan.messenger.entity.UserData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import kotlin.test.assertEquals

class MessagesRepositoryTest {

    private val db = Database.connect(
            url = "jdbc:postgresql:messenger",
            driver = "org.postgresql.Driver",
            user = "postgres",
            password = "admin"
    )

    private val messagesRepository = ExposedMessagesRepository(db)
    private val dialogsRepository = ExposedDialogsRepository(db)
    private val usersRepository = ExposedUsersRepository(db)

    @Test
    fun testCreateMessage() = runBlocking {
        val user1 = usersRepository.createUserAsync(UserData("First Test", "Test")).await()
        val user2 = usersRepository.createUserAsync(UserData("Second Test", "Test")).await()
        val dialog = dialogsRepository.createDialogAsync(DialogData(user1, user2)).await()
        fun onEndAsync() = CoroutineScope(Dispatchers.IO).async {
            dialogsRepository.removeDialogAsync(dialog.id).await()
            usersRepository.removeUserAsync(user1.id).await()
            usersRepository.removeUserAsync(user2.id).await()
        }
        try {
            val message = messagesRepository
                    .createMessageAsync(MessageData(dialog, user1, user2, "Test Message"))
                    .await()
            messagesRepository.removeMessageAsync(message.id).await()
        } catch (exc: Exception) {
            onEndAsync().await()
            fail(exc)
        }
        onEndAsync().await()
        Unit
    }

    @Test
    fun testGetMessageById() = runBlocking {
        val user1 = usersRepository.createUserAsync(UserData("First Test", "Test")).await()
        val user2 = usersRepository.createUserAsync(UserData("Second Test", "Test")).await()
        val dialog = dialogsRepository.createDialogAsync(DialogData(user1, user2)).await()
        val message = messagesRepository
                .createMessageAsync(MessageData(dialog, user2, user1, "Test Message"))
                .await()

        fun onEndAsync() = CoroutineScope(Dispatchers.IO).async {
            messagesRepository.removeMessageAsync(message.id).await()
            dialogsRepository.removeDialogAsync(dialog.id).await()
            usersRepository.removeUserAsync(user1.id).await()
            usersRepository.removeUserAsync(user2.id).await()
        }
        try {
            val result = messagesRepository.getByIdAsync(message.id).await()
            assertEquals("Test Message", result.messageData.text)
            assertEquals(dialog.id, result.messageData.dialog.id)
            assertEquals("First Test",
                    result.messageData.dialog.dialogData.secondUser.userData.firstName)
            assertEquals("Second Test",
                    result.messageData.dialog.dialogData.firstUser.userData.firstName)
            assertEquals("First Test",
                    result.messageData.receiver.userData.firstName)
            assertEquals("Second Test",
                    result.messageData.sender.userData.firstName)
        } catch (exc: Exception) {
            onEndAsync().await()
            fail(exc)
        }
        onEndAsync().await()
        Unit
    }

    @Test
    fun testGetDialogMessages() = runBlocking {
        val user1 = usersRepository.createUserAsync(UserData("First Test", "Test")).await()
        val user2 = usersRepository.createUserAsync(UserData("Second Test", "Test")).await()
        val dialog = dialogsRepository.createDialogAsync(DialogData(user1, user2)).await()
        val message1 = messagesRepository
                .createMessageAsync(MessageData(dialog, user2, user1, "Test Message"))
                .await()
        val message2 = messagesRepository
                .createMessageAsync(MessageData(dialog, user1, user2, "Test Response"))
                .await()

        fun onEndAsync() = CoroutineScope(Dispatchers.IO).async {
            messagesRepository.removeMessageAsync(message1.id).await()
            messagesRepository.removeMessageAsync(message2.id).await()
            dialogsRepository.removeDialogAsync(dialog.id).await()
            usersRepository.removeUserAsync(user1.id).await()
            usersRepository.removeUserAsync(user2.id).await()
        }
        try {
            val messages = messagesRepository.getDialogMessagesAsync(dialog.id).await()
            assertEquals(2, messages.size)
            assertEquals("Test Message", messages[0].messageData.text)
            assertEquals("Test Response", messages[1].messageData.text)
        } catch (exc: Exception) {
            onEndAsync().await()
            fail(exc)
        }
        onEndAsync().await()
        Unit
    }
}
