package com.gitlab.andrewkuryan.messenger.repository

interface MessengerRepository {

    val usersRepository: UsersRepository

    val dialogsRepository: DialogsRepository

    val messagesRepository: MessagesRepository
}
