package com.gitlab.andrewkuryan.messenger.db

import com.gitlab.andrewkuryan.messenger.entity.Message
import com.gitlab.andrewkuryan.messenger.entity.MessageData
import com.gitlab.andrewkuryan.messenger.repository.MessagesRepository
import com.soywiz.klock.DateTime
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

class ExposedMessagesRepository(private val db: Database) : MessagesRepository {

    private val ioScope = CoroutineScope(Dispatchers.IO)

    override fun createMessageAsync(messageData: MessageData): Deferred<Message> {
        return ioScope.async {
            suspendedTransactionAsync(Dispatchers.IO, db) {
                Messages.insert { messageData.toInsertStatement(it) }
                        .toEntityMessage(messageData)
            }.await()
        }
    }

    private fun MessageData.toInsertStatement(statement: InsertStatement<Number>) = this.apply {
        statement[Messages.dialogId] = dialog.id
        statement[Messages.senderId] = sender.id
        statement[Messages.receiverId] = receiver.id
        statement[Messages.text] = text
    }

    private fun InsertStatement<Number>.toEntityMessage(data: MessageData) = Message(
            id = this get Messages.id,
            sendingDate = DateTime((this get Messages.sendingDate).millis),
            messageData = data
    )

    override fun removeMessageAsync(messageId: String) = ioScope.async {
        val message = getByIdAsync(messageId).await()
        suspendedTransactionAsync(Dispatchers.IO, db) {
            Messages.deleteWhere { Messages.id eq messageId }
            message
        }.await()
    }

    override fun getDialogMessagesAsync(dialogId: String) = ioScope.async {
        suspendedTransactionAsync(Dispatchers.IO, db) {
            val sender = Users.alias("sender")
            val receiver = Users.alias("receiver")
            (Messages.join(sender, JoinType.INNER, Messages.senderId, sender[Users.id])
                    .join(receiver, JoinType.INNER, Messages.receiverId, receiver[Users.id])
                    .join(Dialogs, JoinType.INNER, Messages.dialogId, Dialogs.id))
                    .select { Messages.dialogId eq dialogId }
                    .orderBy(Messages.sendingDate)
                    .map { it.toEntityMessage(sender, receiver) }
        }.await()
    }

    override fun getByIdAsync(messageId: String) = ioScope.async {
        suspendedTransactionAsync(Dispatchers.IO, db) {
            val sender = Users.alias("sender")
            val receiver = Users.alias("receiver")
            (Messages.join(sender, JoinType.INNER, Messages.senderId, sender[Users.id])
                    .join(receiver, JoinType.INNER, Messages.receiverId, receiver[Users.id])
                    .join(Dialogs, JoinType.INNER, Messages.dialogId, Dialogs.id))
                    .select { Messages.id eq messageId }
                    .firstOrNull()
                    ?.toEntityMessage(sender, receiver)
                    ?: throw IllegalArgumentException("No such message")
        }.await()
    }
}
