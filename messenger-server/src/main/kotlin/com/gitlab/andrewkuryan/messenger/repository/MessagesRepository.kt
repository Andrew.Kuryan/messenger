package com.gitlab.andrewkuryan.messenger.repository

import com.gitlab.andrewkuryan.messenger.entity.Message
import com.gitlab.andrewkuryan.messenger.entity.MessageData
import kotlinx.coroutines.Deferred

interface MessagesRepository {

    fun createMessageAsync(messageData: MessageData): Deferred<Message>

    fun removeMessageAsync(messageId: String): Deferred<Message>

    fun getDialogMessagesAsync(dialogId: String): Deferred<List<Message>>

    fun getByIdAsync(messageId: String): Deferred<Message>
}
