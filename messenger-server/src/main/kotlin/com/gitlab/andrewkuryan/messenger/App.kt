package com.gitlab.andrewkuryan.messenger

import com.gitlab.andrewkuryan.messenger.db.Dialogs
import com.gitlab.andrewkuryan.messenger.db.Messages
import com.gitlab.andrewkuryan.messenger.db.Users
import io.ktor.application.call
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

fun main(args: Array<String>) {
    val lib = JVMLibrary()

    val db = Database.connect(
            url = "jdbc:postgresql:messenger",
            driver = "org.postgresql.Driver",
            user = "postgres",
            password = "admin"
    )
    transaction(db) {
        SchemaUtils.create(Users, Dialogs, Messages)
    }

    embeddedServer(Netty, 3000) {
        routing {
            get("/") {
                call.respondText("""{
                |    "rootAppName": "${lib.rootAppName}",
                |    "appName": "${lib.appName}",
                |    "distName": "Messenger Server"
                |}""".trimMargin("|"))
            }
        }
    }.start(wait = true)
}
