package com.gitlab.andrewkuryan.messenger.repository

import com.gitlab.andrewkuryan.messenger.entity.Dialog
import com.gitlab.andrewkuryan.messenger.entity.DialogData
import kotlinx.coroutines.Deferred

interface DialogsRepository {

    fun createDialogAsync(dialogData: DialogData): Deferred<Dialog>

    fun removeDialogAsync(dialogId: String): Deferred<Dialog>

    fun getUserDialogsAsync(userId: String): Deferred<List<Dialog>>

    fun getByIdAsync(dialogId: String): Deferred<Dialog>
}
