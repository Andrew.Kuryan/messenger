package com.gitlab.andrewkuryan.messenger.db

import com.gitlab.andrewkuryan.messenger.entity.User
import com.gitlab.andrewkuryan.messenger.entity.UserData
import com.soywiz.klock.DateTime
import me.nimavat.shortid.ShortId
import org.jetbrains.exposed.sql.Alias
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.joda.time.DateTime as SQLDateTime

object Users : Table("users") {
    val id = varchar("id", 14)
            .clientDefault { ShortId.generate() }
            .uniqueIndex()
            .primaryKey()
    val firstName = varchar("first_name", 50)
    val lastName = varchar("last_name", 50)
    val avatarUrl = varchar("avatarUrl", 50)
            .nullable()
    val registeredDate = datetime("registered_date")
            .clientDefault { SQLDateTime.now() }
}

fun ResultRow.toEntityUser() = User(
        this[Users.id],
        DateTime(this[Users.registeredDate].millis),
        UserData(
                this[Users.firstName],
                this[Users.lastName],
                this[Users.avatarUrl]
        )
)

fun ResultRow.toEntityUser(alias: Alias<Users>) = User(
        this[alias[Users.id]],
        DateTime(this[alias[Users.registeredDate]].millis),
        UserData(
                this[alias[Users.firstName]],
                this[alias[Users.lastName]],
                this[alias[Users.avatarUrl]]
        )
)
