package com.gitlab.andrewkuryan.messenger.db

import com.gitlab.andrewkuryan.messenger.entity.*
import com.soywiz.klock.DateTime
import me.nimavat.shortid.ShortId
import org.jetbrains.exposed.sql.Alias
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.joda.time.DateTime as SQLDateTime

object Messages : Table("messages") {
    val id = varchar("id", 14)
            .clientDefault { ShortId.generate() }
            .uniqueIndex()
            .primaryKey()
    val dialogId = reference("dialog_id", Dialogs.id)
    val senderId = reference("sender_id", Users.id)
    val receiverId = reference("receiver_id", Users.id)
    val sendingDate = datetime("sending_date")
            .clientDefault { SQLDateTime.now() }
    val text = text("text")
}

fun ResultRow.toEntityMessage(sender: Alias<Users>, receiver: Alias<Users>) =
        toEntityMessage(toEntityUser(sender), toEntityUser(receiver))

fun ResultRow.toEntityMessage(sender: User, receiver: User) = Message(
        id = this[Messages.id],
        sendingDate = DateTime(this[Messages.sendingDate].millis),
        messageData = MessageData(
                dialog = Dialog(
                        id = this[Dialogs.id],
                        dateCreated = DateTime(this[Dialogs.dateCreated].millis),
                        dialogData = DialogData(sender, receiver)
                ),
                sender = sender,
                receiver = receiver,
                text = this[Messages.text]
        )
)
