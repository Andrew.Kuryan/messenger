package com.gitlab.andrewkuryan.messenger.db

import com.gitlab.andrewkuryan.messenger.entity.User
import com.gitlab.andrewkuryan.messenger.entity.UserData
import com.gitlab.andrewkuryan.messenger.entity.UserFilter
import com.gitlab.andrewkuryan.messenger.repository.UsersRepository
import com.soywiz.klock.DateTime
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.statements.UpdateStatement
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import org.joda.time.DateTime as SQLDateTime

class ExposedUsersRepository(private val db: Database) : UsersRepository {

    private val ioScope = CoroutineScope(Dispatchers.IO)

    override fun createUserAsync(data: UserData) = ioScope.async {
        suspendedTransactionAsync(Dispatchers.IO, db) {
            Users.insert { data.toInsertStatement(it) }
                    .toEntityUser(data)
        }.await()
    }

    private fun UserData.toInsertStatement(statement: InsertStatement<Number>) = this.apply {
        statement[Users.firstName] = firstName
        statement[Users.lastName] = lastName
        statement[Users.avatarUrl] = avatarUrl
    }

    private fun InsertStatement<Number>.toEntityUser(data: UserData) = User(
            id = this get Users.id,
            registeredDate = DateTime((this get Users.registeredDate).millis),
            userData = data
    )

    override fun updateUserAsync(userId: String, newData: UserData) = ioScope.async {
        val oldUser = getByIdAsync(userId).await()
        val newUser = User(oldUser.id, oldUser.registeredDate, newData)
        suspendedTransactionAsync(Dispatchers.IO, db) {
            Users.update({ Users.id eq userId }) {
                newData.toUpdateStatement(it)
            }
            newUser
        }.await()
    }

    private fun UserData.toUpdateStatement(statement: UpdateStatement) = this.apply {
        statement[Users.firstName] = firstName
        statement[Users.lastName] = lastName
        statement[Users.avatarUrl] = avatarUrl
    }

    override fun removeUserAsync(userId: String): Deferred<User> = ioScope.async {
        val user = getByIdAsync(userId).await()
        suspendedTransactionAsync(Dispatchers.IO, db) {
            Users.deleteWhere { Users.id eq userId }
            user
        }.await()
    }

    override fun searchUsersAsync(userFilter: UserFilter) = ioScope.async {
        suspendedTransactionAsync(Dispatchers.IO, db) {
            Users.select { userFilterToWhereOp(userFilter) }
                    .map { it.toEntityUser() }
        }.await()
    }

    override fun getByIdAsync(userId: String) = ioScope.async {
        suspendedTransactionAsync(Dispatchers.IO, db) {
            Users.select { Users.id eq userId }
                    .firstOrNull()
                    ?.toEntityUser()
                    ?: throw IllegalArgumentException("No such user")
        }.await()
    }

    private fun SqlExpressionBuilder.userFilterToWhereOp(filter: UserFilter) =
            Op.build { Op.TRUE }
                    .and(if (filter.namePattern != null)
                        Users.firstName.regexp(filter.namePattern!!.pattern) or
                                Users.lastName.regexp(filter.namePattern!!.pattern)
                    else Op.TRUE)
                    .and(if (filter.registeredDateRange != null)
                        Users.registeredDate.between(
                                SQLDateTime(filter.registeredDateRange!!.from.unixMillisLong),
                                SQLDateTime(filter.registeredDateRange!!.to.unixMillisLong)
                        )
                    else Op.TRUE)
}
