package com.gitlab.andrewkuryan.messenger.repository

import com.gitlab.andrewkuryan.messenger.entity.User
import com.gitlab.andrewkuryan.messenger.entity.UserData
import com.gitlab.andrewkuryan.messenger.entity.UserFilter
import kotlinx.coroutines.Deferred

interface UsersRepository {

    fun createUserAsync(data: UserData): Deferred<User>

    fun updateUserAsync(userId: String, newData: UserData): Deferred<User>

    fun removeUserAsync(userId: String): Deferred<User>

    fun searchUsersAsync(userFilter: UserFilter): Deferred<List<User>>

    fun getByIdAsync(userId: String): Deferred<User>
}
