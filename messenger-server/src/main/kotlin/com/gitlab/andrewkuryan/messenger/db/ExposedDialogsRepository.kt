package com.gitlab.andrewkuryan.messenger.db

import com.gitlab.andrewkuryan.messenger.entity.Dialog
import com.gitlab.andrewkuryan.messenger.entity.DialogData
import com.gitlab.andrewkuryan.messenger.repository.DialogsRepository
import com.soywiz.klock.DateTime
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

class ExposedDialogsRepository(private val db: Database) : DialogsRepository {

    private val ioScope = CoroutineScope(Dispatchers.IO)

    override fun createDialogAsync(dialogData: DialogData) = ioScope.async {
        suspendedTransactionAsync(Dispatchers.IO, db) {
            Dialogs.insert { dialogData.toInsertStatement(it) }
                    .toEntityDialog(dialogData)
        }.await()
    }

    private fun DialogData.toInsertStatement(statement: InsertStatement<Number>) = this.apply {
        statement[Dialogs.firstUserId] = firstUser.id
        statement[Dialogs.secondUserId] = secondUser.id
    }

    private fun InsertStatement<Number>.toEntityDialog(data: DialogData) = Dialog(
            id = this get Dialogs.id,
            dateCreated = DateTime((this get Dialogs.dateCreated).millis),
            dialogData = data
    )

    override fun removeDialogAsync(dialogId: String) = ioScope.async {
        val dialog = getByIdAsync(dialogId).await()
        suspendedTransactionAsync(Dispatchers.IO, db) {
            Dialogs.deleteWhere { Dialogs.id eq dialogId }
            dialog
        }.await()
    }

    override fun getUserDialogsAsync(userId: String) = ioScope.async {
        suspendedTransactionAsync(Dispatchers.IO, db) {
            val firstUser = Users.alias("firstUser")
            val secondUser = Users.alias("secondUser")
            (Dialogs.join(firstUser, JoinType.INNER, Dialogs.firstUserId, firstUser[Users.id])
                    .join(secondUser, JoinType.INNER, Dialogs.secondUserId, secondUser[Users.id]))
                    .select { (Dialogs.firstUserId eq userId) or (Dialogs.secondUserId eq userId) }
                    .map { it.toEntityDialog(firstUser, secondUser) }
        }.await()
    }

    override fun getByIdAsync(dialogId: String) = ioScope.async {
        suspendedTransactionAsync(Dispatchers.IO, db) {
            val firstUser = Users.alias("firstUser")
            val secondUser = Users.alias("secondUser")
            (Dialogs.join(firstUser, JoinType.INNER, Dialogs.firstUserId, firstUser[Users.id])
                    .join(secondUser, JoinType.INNER, Dialogs.secondUserId, secondUser[Users.id]))
                    .select { Dialogs.id eq dialogId }
                    .firstOrNull()
                    ?.toEntityDialog(firstUser, secondUser)
                    ?: throw IllegalArgumentException("No such dialog")
        }.await()
    }
}
