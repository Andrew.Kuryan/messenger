package com.gitlab.andrewkuryan.messenger.db

import com.gitlab.andrewkuryan.messenger.entity.Dialog
import com.gitlab.andrewkuryan.messenger.entity.DialogData
import com.soywiz.klock.DateTime
import me.nimavat.shortid.ShortId
import org.jetbrains.exposed.sql.Alias
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.joda.time.DateTime as SQLDateTime

object Dialogs : Table("dialogs") {
    val id = varchar("id", 14)
            .clientDefault { ShortId.generate() }
            .uniqueIndex()
            .primaryKey()
    val firstUserId = reference("first_user_id", Users.id)
    val secondUserId = reference("second_user_id", Users.id)
    val dateCreated = datetime("date_created")
            .clientDefault { SQLDateTime.now() }
}

fun ResultRow.toEntityDialog(firstUser: Alias<Users>, secondUser: Alias<Users>) =
        Dialog(
                this[Dialogs.id],
                DateTime(this[Dialogs.dateCreated].millis),
                DialogData(toEntityUser(firstUser), toEntityUser(secondUser))
        )
