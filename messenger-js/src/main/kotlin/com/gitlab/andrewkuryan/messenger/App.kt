package com.gitlab.andrewkuryan.messenger

import kotlinx.html.div
import kotlinx.html.dom.append
import kotlinx.html.p
import kotlin.browser.document

class JsApp {
    val rootAppName = CommonLib().appName
    val appName = "Messenger JS"
}

fun main() {
    val app = JsApp()

    document.getElementById("app")?.append {
        div {
            p {
                +app.rootAppName
            }
            p {
                +app.appName
            }
        }
    }
}
