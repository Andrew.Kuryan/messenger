plugins {
    kotlin("js") version "1.3.61"
}

repositories {
    jcenter()
    mavenCentral()
    maven(url = "https://dl.bintray.com/kotlin/kotlin-eap")
    maven(url = "https://dl.bintray.com/kotlin/kotlin-dev")
    maven(url = "https://dl.bintray.com/kotlinx/kotlinx")
    maven(url = "https://dl.bintray.com/kotlin/kotlin-js-wrappers")
}

tasks.create<Exec>("compileStylus") {
    commandLine(
            "../build/js/node_modules/.bin/stylus",
            "src/main/resources/css/index.styl",
            "-o",
            "build/processedResources/Js/main/index.css"
    )
}

tasks.named("processResources") {
    dependsOn("compileStylus")
}

kotlin {
    target {
        browser {
            val main by compilations.getting {
                kotlinOptions {
                    sourceMap = true
                    moduleKind = "commonjs"
                    sourceMapEmbedSources = "always"
                }

                dependencies {
                    implementation(project(":messenger-common"))
                    implementation(kotlin("stdlib-js"))
                    implementation("org.jetbrains.kotlinx:kotlinx-html-js:0.7.1")

                    implementation("org.jetbrains:kotlin-css:1.0.0-pre.91-kotlin-1.3.61")
                    implementation("org.jetbrains:kotlin-css-js:1.0.0-pre.91-kotlin-1.3.61")

                    implementation(npm("stylus", "^0.54.7"))
                }
            }
            runTask {
                sourceMaps = true
                saveEvaluatedConfigFile = true
                devServer = devServer!!.copy(
                        open = false,
                        port = 8083
                )
            }
        }
    }
}
