package com.gitlab.andrewkuryan.messenger.entity

import com.gitlab.andrewkuryan.messenger.extensions.maxValue
import com.gitlab.andrewkuryan.messenger.extensions.minValue
import com.soywiz.klock.DateTime
import com.soywiz.klock.DateTimeRange
import kotlin.js.JsName
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class FilterTest {

    private val testUser = User(
            "1",
            registeredDate = DateTime(2020, 1, 1, 12, 0, 0),
            userData = UserData("Test", "Test")
    )

    @JsName("userFilterPositiveTest")
    @Test
    fun `UserFilter by name regex '*Te*'`() {
        val filter = UserFilter(
                namePattern = Regex(".*Te.*")
        )
        assertTrue(filter.match(testUser))
    }

    @JsName("userFilterNegativeTest")
    @Test
    fun `UserFilter by name regex '*este*'`() {
        val filter = UserFilter(
                namePattern = Regex(".*este.*")
        )
        assertFalse(filter.match(testUser))
    }

    @JsName("userFilterDateRangeTest1")
    @Test
    fun `UserFilter by date range (31-12-2019 - ∞)`() {
        val filter = UserFilter(
                registeredDateRange = DateTimeRange(
                        from = DateTime(2019, 12, 31, 12, 0, 0),
                        to = DateTime.maxValue
                )
        )
        assertTrue(filter.match(testUser))
    }

    @JsName("userFilterDateRangeTest2")
    @Test
    fun `UserFilter by date range (-∞ - 01-02-2020)`() {
        val filter = UserFilter(
                registeredDateRange = DateTimeRange(
                        from = DateTime.minValue,
                        to = DateTime(2020, 2, 1, 12, 0, 0)
                )
        )
        assertTrue(filter.match(testUser))
    }

    @JsName("userFilterDateRangeAndNamePatternTest")
    @Test
    fun `UserFilter by name pattern and date range`() {
        val filter = UserFilter(
                namePattern = Regex(".*es.*"),
                registeredDateRange = DateTimeRange(
                        from = DateTime(2019, 12, 31, 12, 0, 0),
                        to = DateTime(2020, 2, 1, 12, 0, 0)
                )
        )
        assertTrue(filter.match(testUser))
    }
}
