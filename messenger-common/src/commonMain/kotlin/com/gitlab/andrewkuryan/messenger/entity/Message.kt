package com.gitlab.andrewkuryan.messenger.entity

import com.soywiz.klock.DateTime

data class MessageData(
        val dialog: Dialog,
        val sender: User,
        val receiver: User,
        val text: String
)

data class Message(
        val id: String,
        val sendingDate: DateTime,
        val messageData: MessageData
)
