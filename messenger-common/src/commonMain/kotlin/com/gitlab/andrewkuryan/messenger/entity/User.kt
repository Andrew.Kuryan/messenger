package com.gitlab.andrewkuryan.messenger.entity

import com.soywiz.klock.DateTime
import com.soywiz.klock.DateTimeRange

data class UserData(
        val firstName: String,
        val lastName: String,
        val avatarUrl: String? = null
)

data class User(
        val id: String,
        val registeredDate: DateTime,
        val userData: UserData
)

data class UserFilter(
        val namePattern: Regex? = null,
        val registeredDateRange: DateTimeRange? = null
) {

    fun match(user: User) =
            (namePattern == null
                    || user.userData.firstName.matches(namePattern)
                    || user.userData.lastName.matches(namePattern))
                    && (registeredDateRange == null
                    || registeredDateRange.contains(user.registeredDate))
}
