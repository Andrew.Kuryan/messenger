package com.gitlab.andrewkuryan.messenger.extensions

import com.soywiz.klock.DateTime

val DateTime.Companion.minValue: DateTime
    get() = DateTime(0L)

val DateTime.Companion.maxValue: DateTime
    get() = DateTime(Long.MAX_VALUE)
