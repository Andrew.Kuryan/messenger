package com.gitlab.andrewkuryan.messenger.entity

import com.soywiz.klock.DateTime

data class DialogData(
        val firstUser: User,
        val secondUser: User
)

data class Dialog(
        val id: String,
        val dateCreated: DateTime,
        val dialogData: DialogData
)
